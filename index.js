// ==UserScript==
// @name         Genshin Web Event Solver
// @version      0.1
// @description  Solves Genshin Events
// @author       Eloncase
// @namespace    https://gitlab.com/Eloncase/genshin-web-event-solver
// @match        https://act.hoyoverse.com/ys/event/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=act.hoyoverse.com
// @grant        GM_xmlhttpRequest
// @grant        GM_addStyle
// @connect      sg-hk4e-api.hoyoverse.com
// @connect      webapi-os.account.hoyoverse.com
// ==/UserScript==

(function() {
    'use strict';

    GM_addStyle(".elon-event-box { position: absolute; right: 0; bottom: 0; display: flex; flex-direction: column; padding: 10px; background-color: #0001; pointer-events: none; }");
    GM_addStyle(".elon-event-box .event-status.fail { color: red; }");

    let statusBox = null;
    let lastStatus = null;
    attachStatusBox();

    const events = {
        '/ys/event/e20221028nahida/index.html': f_e20221028nahida
    };

    writeStatus("Init");
    writeLog(`Event page: ${location.pathname}`);

    let solution = events[location.pathname];
    if (solution) {
        writeStatus("Found solution");
        setTimeout(solution, 1000);
    } else {
        writeStatus("No solution");
        return;
    }

    function f_e20221028nahida() {
        writeStatus("Checking login");
        let uid = 0;

        checkLogin();

        function checkLogin() {
            let loginDiv = document.getElementsByClassName("mihoyo-account-role__uid");
            if (!loginDiv.length) {
                writeStatus("Please login");
            } else {
                uid = loginDiv[0].innerText.slice("UID: ".length);
                checkStatus();
                return;
            }
            setTimeout(checkLogin, 1000);
        }

        function checkStatus() {
            writeStatus("Checking event status");
            GM_xmlhttpRequest({
                method: 'GET',
                url: `https://sg-hk4e-api.hoyoverse.com/event/merlin_v2/v3/flow/run/hk4e_global/e20221028nasita/1?lang=en-us&uid=${uid}&region=os_euro&game_biz=hk4e_global`,
                fetch: true,
                responseType: 'json',
                onload: onCheckStatus
            });
        }

        function onCheckStatus(response) {
            let hoyoResponse = response.response;
            if (hoyoResponse.retcode != 0) {
                writeStatus("Failed to get status, check console for more info", true);
                writeLog(hoyoResponse);
                return;
            } else {
                if (hoyoResponse.data.is_reward) {
                    writeStatus("Claimed reward already");
                    return;
                } else {
                    postProgress();
                }
            }
        }

        function postProgress() {
            writeStatus("Posting event progress");
            GM_xmlhttpRequest({
                method: 'POST',
                url: `https://sg-hk4e-api.hoyoverse.com/event/merlin_v2/v3/flow/run/hk4e_global/e20221028nasita/3?lang=en-us&uid=${uid}&region=os_euro&game_biz=hk4e_global`,
                fetch: true,
                responseType: 'json',
                data: JSON.stringify({ first_stage: 1, second_stage: 1, third_stage: 1 }),
                onload: onPostProgress
            });
        }

        function onPostProgress(response) {
            let hoyoResponse = response.response;
            if (hoyoResponse.retcode != 0) {
                writeStatus("Failed to post progress, check console for more info", true);
                writeLog(hoyoResponse);
                return;
            } else {
                writeStatus("Posted progress");
                claimReward();
            }
        }

        function claimReward() {
            writeStatus("Claiming rewards");
            GM_xmlhttpRequest({
                method: 'POST',
                url: `https://sg-hk4e-api.hoyoverse.com/event/merlin_v2/v3/flow/run/hk4e_global/e20221028nasita/5?lang=en-us&uid=${uid}&region=os_euro&game_biz=hk4e_global`,
                fetch: true,
                responseType: 'json',
                onload: onClaimReward
            });
        }

        function onClaimReward(response) {
            let hoyoResponse = response.response;
            if (hoyoResponse.retcode != 0) {
                writeStatus("Failed to claim rewards, check console for more info", true);
                writeLog(hoyoResponse);
                return;
            } else {
                writeStatus("Claimed rewards");
                postShare();
            }
        }

        function postShare() {
            writeStatus("Posting share");
            GM_xmlhttpRequest({
                method: 'POST',
                url: `https://sg-hk4e-api.hoyoverse.com/event/merlin_v2/v3/flow/run/hk4e_global/e20221028nasita/4?lang=en-us&uid=${uid}&region=os_euro&game_biz=hk4e_global`,
                fetch: true,
                responseType: 'json',
                onload: onPostShare
            });
        }

        function onPostShare(response) {
            let hoyoResponse = response.response;
            if (hoyoResponse.retcode != 0) {
                writeStatus("Failed to post share, check console for more info", true);
                writeLog(hoyoResponse);
                return;
            } else {
                writeStatus("Done");
            }
        }
    }

    function writeLog()
    {
        console.log('%c[GenshEvent]%c', 'color:#9fc5e8; font-weight:bold;', '', ...arguments);
    }

    function writeStatus(message, fail)
    {
        if (message == lastStatus) return;
        lastStatus = message;
        writeLog(message);
        if (!statusBox) return;
        statusBox.innerText = message;
        if (fail) {
            statusBox.classList.add("fail");
        } else {
            statusBox.classList.remove("fail");
        }
    }

    function attachStatusBox() {
        let div = document.createElement("div");
        div.classList.add("elon-event-box");
        let span = document.createElement("span");
        span.innerText = "Eloncase Event Solver";
        div.append(span);
        span = document.createElement("span");
        span.innerText = "Status: ";
        div.append(span);
        statusBox = document.createElement("span");
        statusBox.classList.add("event-status");
        span.append(statusBox);
        document.body.append(div);
    }
})();
